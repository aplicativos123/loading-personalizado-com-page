import { Injectable } from '@angular/core';
import { App } from "ionic-angular";

@Injectable()

export class LoadingPageProvider {

  constructor(private app:App) {
    console.log('Hello LoadingPageProvider Provider');
   
  }

  loading(){
    this.app.getRootNav().push('MeuLoadingPage');
  }

  loadingHide(){
    this.app.getRootNav().pop();
  }

}
