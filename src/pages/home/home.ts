import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingPageProvider } from '../../providers/loading-page/loading-page';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public loadingProvider: LoadingPageProvider) {

  }

  clickBotao(){
    this.loadingProvider.loading(); 

    setTimeout(()=>{ 
      this.loadingProvider.loadingHide();
    }, 1000);
  }

  clickBotao2(){
    this.navCtrl.push('ContatosPage');
  }
 
}
