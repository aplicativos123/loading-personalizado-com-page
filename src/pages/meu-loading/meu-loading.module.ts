import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeuLoadingPage } from './meu-loading';

@NgModule({
  declarations: [
    MeuLoadingPage,
  ],
  imports: [
    IonicPageModule.forChild(MeuLoadingPage),
  ],
})
export class MeuLoadingPageModule {}
