import { IonicPage} from 'ionic-angular';
import { Component, Injectable } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-meu-loading',
  templateUrl: 'meu-loading.html',
})
export class MeuLoadingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoadingPage');
  }
}
